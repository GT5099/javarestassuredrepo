package restAssuredMethods;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class getMethod1 {
	// Bitbucket@5099 - Passwords
	//https://bitbucket.org/GT5099/javarestassuredrepo/src/master/
	@Test
	public void getMethodTest()
	{
		//Build the request
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking/{id}")
				.pathParam("id", 2)
		//hit the request
			.when()
				.get()
		//validate the response
			.then()
				.log()
				.all()
				.statusCode(200);
	}

}
